FROM golang

ADD . /go/src/github.com/jrecuerda/tls_web_sample

RUN go install github.com/jrecuerda/tls_web_sample 

ENTRYPOINT ["/go/bin/tls_web_sample"]
CMD ["/go/src/github.com/jrecuerda/tls_web_sample/keys"]

# Document that the service listens on port 8080.
EXPOSE 8080
