package main

import (
	"fmt"
)

type output struct {
	mark float32
	date string
}

func (o output) Format() string {
	return fmt.Sprintf("<label> NOTA-->%f FECHA-->%s </label>\n", o.mark, o.date)
}
