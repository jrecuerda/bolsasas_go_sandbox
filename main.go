package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func replyMark(w http.ResponseWriter) {
	mark, date := GetMarkDate(float32(100))
	output := output{mark: mark, date: date}
	w.Write([]byte(output.Format()))

}

func mainHandler(w http.ResponseWriter, req *http.Request) {
	// Take a rest
	rest_time, _ := time.ParseDuration("1s")
	time.Sleep(rest_time)
	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	replyMark(w)
}

func launchServer(keys_folder string) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", mainHandler)
	cfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
	srv := &http.Server{
		Addr:         ":8080",
		Handler:      mux,
		TLSConfig:    cfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}
	log.Fatal(srv.ListenAndServeTLS(fmt.Sprintf("%s/server.crt", keys_folder), fmt.Sprintf("%s/server.key", keys_folder)))

}

func main() {
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) != 1 {
		panic("Invalid number of arguments")
	}
	launchServer(argsWithoutProg[0])
}
