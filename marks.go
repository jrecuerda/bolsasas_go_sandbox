package main

import (
	"fmt"
	"math/rand"
	"time"
)

// GetMarkDate Get random mark and date
func GetMarkDate(higherLimit float32) (float32, string) {
	return (rand.Float32() * higherLimit), getDate()
}

func getDate() string {
	// Today plus 1 year
	moved_date := time.Now().AddDate(1, 0, 0)
	//year, month, day := time.Now().Date()
	return fmt.Sprintf("%d/%d/%d", moved_date.Day(), int(moved_date.Month()), moved_date.Year())
}
